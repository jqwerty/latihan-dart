import 'dart:io';

void main(){
  bool status = true;
  while(status) {
    stdout.write('Masukkan Angka Pertama: ');
    int angkaPertama = int.parse(stdin.readLineSync());
    stdout.write('Masukkan Angka Kedua: ');
    int angkaKedua = int.parse(stdin.readLineSync());
    stdout.write('Masukkan Operasi Matematika: ');
    var operasi = stdin.readLineSync();

    var hasil;
    if(operasi == '+'){
      hasil = angkaPertama + angkaKedua;
    }else if(operasi == '-'){
      hasil = angkaPertama - angkaKedua;
    }else if(operasi == '/'){
      hasil = angkaPertama / angkaKedua;
    }else if(operasi == '*'){
      hasil = angkaPertama * angkaKedua;
    }

    print("Hasil: $hasil");
    stdout.write('Hitung lagi? [Y/n] ');
    var act = stdin.readLineSync();
    if(act == 'Y'){
      status = true;
    }else if(act == 'n'){
      print("Sampai Jumpa!");
      status = false;
      exit(0);
    }else {
      print("Invalid input!");
      status = false;
      exit(0);
    }
  }
}