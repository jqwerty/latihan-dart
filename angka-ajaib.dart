import 'dart:io';
import 'dart:math';

void main(){
  bool status = true;
  while(status){
    print('====== ANGKA AJAIB =====');

    var rng = new Random();
    var budi = rng.nextInt(50);

    bool mantap = true;
    for(var i=1;i<=5;i++){
      if(mantap == true){
      stdout.write("Tebak angka: ");
      var inputNum = int.parse(stdin.readLineSync());

      if(inputNum < budi){
        print("Angka Anda lebih besar");
      }else if(inputNum > budi){
        print("Angka Anda lebih kecil");
      }else if(inputNum == budi){
        print("Hebat! Angka ajaib $budi berhasil Anda tebak dalam $i kali saja!");
        mantap = false;
        break;
      }
    }
  }

    stdout.write("Main lagi? [Y/n] ");
    var decision = stdin.readLineSync();
    if(decision == 'Y'){
      status = true;
    }else if(decision == 'n'){
      print("Selamat Tinggal!");
      status = false;
      exit(0);
    }else {
      print("Invalid input!");
      status = false;
      exit(0);
    }
  }
}